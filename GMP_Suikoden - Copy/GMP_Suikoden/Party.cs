﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMP_Suikoden
{
    public class Party
    {
        /// <summary>
        /// Use this to give the party some kind of identity
        /// </summary>
        public string Name { get; set; }

        // DO NOT CHANGE THESE 2 LINES
        private List<Unit> units = new List<Unit>();
        public IEnumerable<Unit> Units { get { return units; } }

        /// <summary>
        /// If there's no more units left (units list count is 0), return true. False, otherwise
        /// </summary>
        public bool Wiped
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Add the unit to the 'units' list
        /// </summary>
        /// <param name="unit"></param>
        public void AddUnit(Unit unit)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the next unit in queue. This unit is the index 0 of the 'units' list. 
        /// Remove the returned unit from the list.
        /// </summary>
        /// <returns>If the list is empty, return null. Returns the unit at index 0 if the list has at least one unit.</returns>
        public Unit NextUnit()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Print team name and all the units in the party (Console.WriteLine)
        /// </summary>
        public void DisplayParty()
        {
            throw new NotImplementedException();
        }
    }
}
