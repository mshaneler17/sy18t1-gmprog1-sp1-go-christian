﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMP_Suikoden
{
    class Program
    {
        // 1. Initialize the 2 parties

        // 2. Display the 2 parties (use Party.DisplayParty)

        // 3. Loop until at least one party is wiped

        // 3.1 Get the combatants (use Party.NextUnit())

        // 3.2 Loop until at least one unit is dead
        // 3.2.1 Display the units in battle (use Unit.DisplayStats())
        // 3.2.1 Set action for both units (use Unit.SetUserAction and Unit.SetRandomAction)
        // 3.2.2 Evaluate actions of the 2 units

        // 4. Evaluate the winner (may result in a win, lose, or draw)

        // NOTE: Take advantage of the Player and Unit object. Call the object's function whenever applicable.
        // NOTE: Please use functions to implement the listed items
    }
}
